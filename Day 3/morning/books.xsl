<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:template match="/">
		<html>
			<head>
				<title>Books</title>
			</head>
			<body>
				<h1>Here's my books</h1>
				<table cellpadding="0" cellspacing="0" border="1">
					<tbody>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Isbn Number</th>
						</tr>
						
						<!-- here's my for-each loop.  Target the repeating element -->
						<xsl:for-each select="books/book">
							<!-- new table for my new book -->
							<tr>
								<td>
									<xsl:value-of select="title"/>
								</td>
								<td>
									<xsl:value-of select="author"/>
								</td>
								<td>
									<xsl:value-of select="@isbnNumber"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

