<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- in an xsl stylesheet, the first line you need to add to ensure
that a transformation will happen is this one... -->
	<xsl:template match="/">
		<!-- this '/' tells the transformer to look to the root element
		the xml file and start processing there -->
		
		<!-- add some literal code -->
		<html>
			<head>
				<title>Hello Andrew!</title>
			</head>
			<body>
				<!-- using xpath to get an element with some data.
				xpath/xsl will go through the xml and find data in matching element(s)
				<sait> -> <courses> -> <course>  find the data there and output it -->
				<xsl:value-of select="sait/courses/course"/>
				<br/>
				<xsl:value-of select="sait/courses/course/@data"/>
			</body>
		</html>
	
	</xsl:template>
</xsl:stylesheet>
