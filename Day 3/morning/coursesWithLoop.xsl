<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- in an xsl stylesheet, the first line you need to add to ensure
that a transformation will happen is this one... -->
	<xsl:template match="/">
		<!-- this '/' tells the transformer to look to the root element
		the xml file and start processing there -->
		
		<!-- add some literal code -->
		<html>
			<head>
				<title>Hello Andrew!</title>
			</head>
			<body>
				<!-- using xpath to get an element with some data.
				xpath/xsl will go through the xml and find data in matching element(s)
				<sait> -> <courses> -> <course>  find the data there and output it -->
				<xsl:value-of select="sait/courses/course"/>
				<br/>
				<xsl:value-of select="sait/courses/course/@data"/>
				<br/>
				<br/>
				<table>
					<tbody>
						<tr>
							<th>Course Code</th>
							<th>Name</th>
							<th>Instructor</th>
						</tr>
						<!-- target your for-each loop to the xml element that repeats. -->
						<xsl:for-each select="sait/courses/course">
							<!-- create a new table row for each course -->
							<tr>
								<td>
								<!-- notice, because i'm inside the for-each loop,
								I don't need to do this sait/courses/course/code to 
								output the data -->
									<xsl:value-of select="code"/>
								</td>
								<td>
									<xsl:value-of select="name"/>
								</td>
								<td>
									<xsl:value-of select="instructor"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	
	</xsl:template>
</xsl:stylesheet>



